const express = require('express')
const app = express()
const port = 8000

users = []


app.use(express.json())


app.post("/signup", (req, res) => {
	
	if(req.body.username != '' && req.body.password != ''){
		users.push(req.body);
		console.log(users);
		
		res.send(`User ${req.body.username} successfully registered.`);
	} else {

		res.send("Please input BOTH username and password");
	}

});

app.put("/change-password", (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated.`

			break;

		} else {

			message = `User does not exist`;
		}

	}

	res.send(message);


});

app.get('/home', (req,res)=> {
	res.send("Hello welcome to home page.")
})

app.get('/users', (req,res) => {
	res.send(users)
})

app.delete('/delete-user', (req,res) => {
	let updatedUsers = users.filter((user)=> {
		return user.username !== req.body.username
	})

	users = [...updatedUsers]
	console.log(users)
	res.send(users)
})

app.listen(port)

console.log("Server running at port " + port)